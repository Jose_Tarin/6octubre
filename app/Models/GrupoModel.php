<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of GrupoModel
 *
 * @author JoseTarin
 */
namespace App\Models;
use CodeIgniter\Model;

class GrupoModel extends Model {
    protected $table = 'alumnos';
    protected $primaryKey = 'NIA';
    protected $returnType = 'object';
}
